# Wave Function Collapse

Algorithme inspiré des principes de la physique quantique, utile pour la génération aléatoire de niveaux.

Le script `wfc.py` est une implémentation simple pour générer un réseau de tuyaux selon les images dans le répertoire `tiles`.

Pour plus d'information, voir le dépôt officiel : https://github.com/mxgmn/WaveFunctionCollapse
