import numpy as np
from imageio import imread
from random import randint, choice
import matplotlib.pyplot as plt





# objet vignette
class Tile:
    def __init__(self, img, top, bottom, left, right):
        self.img = img[:,:,:3] / 255.0
        self.width = img.shape[1]
        self.height = img.shape[0]
        self.top = top
        self.bottom = bottom
        self.left = left
        self.right = right











# ~~~~~~~~~~~~~~~~~~~~~~~~ initialisation ~~~~~~~~~~~~~~~~~~~~~~~~ #

# chargement de toutes les vignettes et leurs contraintes
images = [imread(f"tiles/tile{i}.png") for i in range(9)]
tiles = [
    Tile(images[0], top=0, bottom=0, left=0, right=0),
    Tile(images[1], top=1, bottom=0, left=0, right=1),
    Tile(images[2], top=0, bottom=1, left=0, right=1),
    Tile(images[3], top=0, bottom=1, left=1, right=0),
    Tile(images[4], top=1, bottom=0, left=1, right=0),
    Tile(images[5], top=1, bottom=1, left=1, right=1),
    Tile(images[6], top=1, bottom=1, left=1, right=1),
    Tile(images[7], top=0, bottom=0, left=1, right=1),
    Tile(images[8], top=1, bottom=1, left=0, right=0)
]

# définition de la liste des vignettes compatibles pour chaque côté
for t in tiles :
    t.top_compatible    = np.array([t.top    == t_.bottom for t_ in tiles])
    t.bottom_compatible = np.array([t.bottom == t_.top    for t_ in tiles])
    t.left_compatible   = np.array([t.left   == t_.right  for t_ in tiles])
    t.right_compatible  = np.array([t.right  == t_.left   for t_ in tiles])

# taille de la grille
X = 20
Y = 20
N = len(tiles)

wave = np.ones((X, Y, N))










# ~~~~~~~~~~~~~~~~~~~~~~~~ observation ~~~~~~~~~~~~~~~~~~~~~~~~ #

indexes = np.array([[(x, y) for y in range(Y)] for x in range(X)]).reshape((-1, 2))

def observe() :
    counts = wave.sum(axis=2).flatten()
    counts[counts == 1] = N+1
    mins = indexes[counts == counts.min()]
    x, y = choice(mins)
    i = choice([i for i in range(N) if wave[x,y,i]])
    wave[x,y][:i] = 0
    wave[x,y][i+1:] = 0
    return x, y











# ~~~~~~~~~~~~~~~~~~~~~~~~ propagation ~~~~~~~~~~~~~~~~~~~~~~~~ #

def propagate_top(x, y) :
    compatible = sum([tiles[i].top_compatible for i in range(N) if wave[x,y,i]]) >= 1
    new_top = wave[x-1, y] * compatible
    if np.sum(new_top != wave[x-1, y]) > 0 :
        wave[x-1, y] = new_top
        propagate(x-1, y)

def propagate_bottom(x, y) :
    compatible = sum([tiles[i].bottom_compatible for i in range(N) if wave[x,y,i]]) >= 1
    new_bottom = wave[x+1, y] * compatible
    if np.sum(new_bottom != wave[x+1, y]) > 0 :
        wave[x+1, y] = new_bottom
        propagate(x+1, y)

def propagate_left(x, y) :
    compatible = sum([tiles[i].left_compatible for i in range(N) if wave[x,y,i]]) >= 1
    new_left = wave[x, y-1] * compatible
    if np.sum(new_left != wave[x, y-1]) > 0 :
        wave[x, y-1] = new_left
        propagate(x, y-1)

def propagate_right(x, y) :
    compatible = sum([tiles[i].right_compatible for i in range(N) if wave[x,y,i]]) >= 1
    new_right = wave[x, y+1] * compatible
    if np.sum(new_right != wave[x, y+1]) > 0 :
        wave[x, y+1] = new_right
        propagate(x, y+1)

def propagate(x, y) :
    if x > 0 : propagate_top(x, y)
    if x < X-1 : propagate_bottom(x, y)
    if y > 0 : propagate_left(x, y)
    if y < Y-1 : propagate_right(x, y)









# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ affichage ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ #

fig, ax = plt.subplots()

def image_at(x, y):
    return sum([wave[x, y, i] * tiles[i].img for i in range(N)]) / wave[x,y].sum()

def render() :
    x_size = tiles[0].height
    y_size = tiles[0].width
    image = np.zeros((x_size*X, y_size*Y, 3))
    for x in range(X) :
        for y in range(Y) :
            image[x*x_size:(x+1)*x_size, y*y_size:(y+1)*y_size] = image_at(x, y)
    ax.imshow(image)
    return image


def press(event):
    x, y = observe()
    propagate(x, y)
    render()
    fig.canvas.draw()

fig.canvas.mpl_connect('key_press_event', press)
render()
plt.show()





# ~~~~~~~~~~~~~~~~~~~~~~~~ boucle principale ~~~~~~~~~~~~~~~~~~~~~~~~ #
"""
while np.sum(wave.sum(axis=2) > 1) :
    x, y = observe()
    propagate(x, y)

render()
"""





