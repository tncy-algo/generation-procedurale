import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np

fig = plt.figure()
ax = fig.gca(projection='3d')

n = 512
X = np.array(range(n))
Y = np.array(range(n))
X, Y = np.meshgrid(X, Y)

# Random Fourier Transform
noise = np.fft.fft2(np.random.random([n,n]))

# Create a filter
filt = np.zeros([n,n])
β = 1.3
for x in range(n) :
    for y in range(n) :
        filt[x,y] = 1/(1+x**2+y**2)**β

# Invser Fourier Transform
Z = abs(np.fft.ifft2(noise*filt))
Z -= Z.min()
Z /= Z.max()

# Plot the surface
surf = ax.plot_surface(X, Y, Z, cmap="bone",
                       linewidth=0, antialiased=True)
ax.set_zlim(-1, 2)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

# Add a color bar which maps values to colors.
fig.colorbar(surf, shrink=0.5, aspect=5)

# Show me all.
plt.show()
