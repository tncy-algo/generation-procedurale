import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np

def randomVector() :
    theta = 2*np.pi*np.random.rand()
    return np.cos(theta), np.sin(theta)

def makeGradient(width, height) :
    Gradient = np.zeros([width, height, 2])
    for x in range(width) :
        for y in range(height) :
            Gradient[x,y,0], Gradient[x,y,1] = randomVector()
    return Gradient

def lerp(a0, a1, w) :
    return (1-w)*a0 + w*a1

def dotGridGradient(ix, iy, x, y) :
    global Gradient
    dx = x - ix
    dy = y - iy
    return (dx*Gradient[ix][iy][0] + dy*Gradient[iy][ix][1])

def perlin(x, y) :
    # Coordonnées (x,y) des 4 noeuds de la case
    # dans laquelle se situe le point
    x0 = int(x)
    x1 = x0+1
    y0 = int(y)
    y1 = y0+1

    # Distance avec le noeud en haut à gauche
    # (qui servira à se situer par rapport au
    # noeud en haut à gauche)
    sx = x - x0
    sy = y - y0

    # Calcul des produits scalaires avec les 4 noeuds
    n0 = dotGridGradient(x0, y0, x, y)
    n1 = dotGridGradient(x1, y0, x, y)
    n2 = dotGridGradient(x0, y1, x, y)
    n3 = dotGridGradient(x1, y1, x, y)

    # Interpolation
    ix0 = lerp(n0, n1, sx)
    ix1 = lerp(n2, n3, sx)
    value = lerp(ix0, ix1, sy)
    
    return value

def fractalPerlin(steps) :
    global width, height
    PerlinSum = np.zeros([width, height])
    
    for n in range(steps) :
        PerlinNoise = np.zeros([width, height])
        z = float(width/2**n)
        for x in range(width) :
            for y in range(height) :
                PerlinNoise[x,y] = perlin(x/z, y/z)
        plt.show()
        PerlinSum += PerlinNoise/2**n
    return PerlinSum

width, height = 500, 500
Gradient = makeGradient(width, height)

Noise = fractalPerlin(8)

# Affichage 2D
plt.imshow(Noise, cmap="bone")
plt.show()

# Affichage 3D
fig = plt.figure()
ax = fig.gca(projection='3d')
X = np.array(range(width))
Y = np.array(range(height))
X, Y = np.meshgrid(X, Y)
surf = ax.plot_surface(X, Y, Noise, cmap="bone",
                       linewidth=0, antialiased=True)
ax.set_zlim(-1, 1)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))
fig.colorbar(surf, shrink=0.5, aspect=5)
plt.show()
