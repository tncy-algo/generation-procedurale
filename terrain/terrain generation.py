# Import a library of functions called 'pygame'
import pygame
from random import random
from time import sleep

pygame.init()

BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)

coo = [0,0]
 
# Set the height and width of the screen
size = (1024, 400)
screen = pygame.display.set_mode(size)
 
pygame.display.set_caption("Une ligne montagneuse")

screen.fill(WHITE)
 
for boucles in range(10) :

    longueur = 1024/(len(coo)-1)
    
    screen.fill(WHITE)

    for i in range(len(coo)-1) :
        pos1 = [i*longueur, 200+coo[i]]
        pos2 = [(i+1)*longueur, 200+coo[i+1]]
        pygame.draw.line(screen, BLACK, pos1, pos2, 2)

    temp = []
    for i in range(len(coo)-1) :
        temp.append(coo[i])
        temp.append(coo[i]-((coo[i]-coo[i+1])/2))
    temp.append(coo[-1])
    coo = []
    for i in temp :
        coo.append(i)

    for i in range(len(coo)) :
        coo[i]+=(random()-0.5)*300/2**boucles

    pygame.display.flip()
    done = False
    while not done :
        for event in pygame.event.get(): 
            if event.type == pygame.KEYUP: 
                done = True
        sleep(0.1)

pygame.display.flip()
done = False
while not done :
    for event in pygame.event.get(): 
        if event.type == pygame.QUIT: 
            done = True
    sleep(0.1)

# Yoloswag
pygame.quit()
