# Génération de terrain

Différentes méthodes de génération sont abordées, parmi lesquelles :
 - L'algorithme de Diamant-Carré
 - Le bruit de Perlin
 - Le simplex noise
 - La synthèse de Fourier
