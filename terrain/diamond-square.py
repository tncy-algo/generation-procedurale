import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.ticker import LinearLocator, FormatStrFormatter
import numpy as np

fig = plt.figure()
ax = fig.gca(projection='3d')

# Make data.
n = 9
X = np.array(range(2**n+1))
Y = np.array(range(2**n+1))
X, Y = np.meshgrid(X, Y)

#Z = np.random.random([2**n+1, 2**n+1])
Z = np.zeros([2**n+1, 2**n+1])

for k in range(n) :
    step = 2**(n-k-1)
    # Diamond phase
    for x in range(step, 2**n, 2*step) :
        for y in range(step, 2**n, 2*step) :
            Z[x,y] = (Z[x-step, y-step] +
                      Z[x-step, y+step] +
                      Z[x+step, y-step] +
                      Z[x+step, y+step]) / 4
    # Square phase
    padding = 0
    for x in range(0, 2**n+1, step) :
        if padding == 0 : padding = step
        else : padding = 0
        for y in range(padding, 2**n+1, 2*step) :
            s = 0
            c = 0
            if x >= step :
                s += Z[x-step, y]
                c += 1
            if x < 2**n+1 - step :
                s += Z[x+step, y]
                c += 1
            if y >= step :
                s += Z[x, y-step]
                c += 1
            if y < 2**n+1 - step :
                s += Z[x, y+step]
                c += 1
            Z[x, y] = s/c
    # Random add
    Z += np.random.random([2**n+1, 2**n+1])/2**(k+1)

# Plot the surface.
surf = ax.plot_surface(X, Y, Z, cmap="bone",
                       linewidth=0, antialiased=True)

# Customize the z axis.
ax.set_zlim(0, 2)
ax.zaxis.set_major_locator(LinearLocator(10))
ax.zaxis.set_major_formatter(FormatStrFormatter('%.02f'))

# Add a color bar which maps values to colors.
fig.colorbar(surf, shrink=0.5, aspect=5)

# Show me all.
plt.show()
