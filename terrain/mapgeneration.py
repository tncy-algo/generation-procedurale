# Import a library of functions called 'pygame'
import pygame, os
from math import log
from random import random
from time import sleep

pygame.init()

T = 2**10
CreerFichier = False
zoom = -2
bT = int(log(T)/log(2)+0.1)+zoom

BLEU =  ( 58, 173, 211)
VERT = ( 88, 152,  60)
JAUNE = ( 213, 200, 111)

size = (T, T)

coo = []

for i in range(2**(-zoom+1)+1):
    coo.append([])
    for j in range(2**(-zoom+1)+1):
        coo[i].append(0)

boucles = 0

for x in range(len(coo)) :
    for y in range(len(coo[x])) :
        coo[x][y] += (random()-0.5)*40

done = False
fini = False

#Generation de la map avec les listes
for boucles in range(1,bT) :

    for i in range(1, len(coo)) :
        coo.insert(2*i-1, [])
        for j in range(len(coo[0])) :
            coo[2*i-1].append((coo[2*i][j]+coo[2*i-2][j])/2)

    for i in range(len(coo)) :
        for j in range(1, len(coo[i])) :
            coo[i].insert(2*j-1, (coo[i][2*j-2]+coo[i][2*j-1])/2)

    for x in range(len(coo)) :
        for y in range(len(coo[x])) :
            coo[x][y] += (random()-0.5)*40/2.05**boucles
        

screen = pygame.display.set_mode(size)
pygame.display.set_caption("Map generation")
screen.fill(BLEU)

for x in range(T) :
    for y in range(T) :
        couleur = int(coo[x][y])
        if couleur > 0 :
            pygame.draw.line(screen, (91-couleur*1.5, 155-couleur*1.5 ,63-couleur*1.5), [x, y], [x, y], 1)
            if couleur < 2 :
                pygame.draw.line(screen, JAUNE, [x, y], [x, y], 1)

pygame.display.flip()

if CreerFichier :
    mapiso = "[iso]"+"\n"
    for x in range(T) :
        for y in range(T) :
            if int(coo[x][y]) == 0 and not coo[x][y] < 0 :
                coo[x][y] = int(coo[x][y])
                mapiso += "v"+str(T*x+y)+"="+str(x)+","+str(y)+",0,2"
            if coo[x][y] < 0 :
                mapiso += "v"+str(T*x+y)+"="+str(x)+","+str(y)+",0,6"
            if int(coo[x][y]) > 0 :
                coo[x][y] = int(coo[x][y])
                mapiso += "v"+str(T*x+y)+"="+str(x)+","+str(y)+",0,4"
                for i in range(1, coo[x][y]+1) :
                    mapiso += ","+str(i)+",4"
            mapiso += "\n"
    mapiso += "no=0\nlo=0\nvo="+str(T**2)

    mapfichier = open("Monde_resultat.iso", "w")
    mapfichier.write(mapiso)
    mapfichier.close()


while not done :

    for event in pygame.event.get(): 
        if event.type == pygame.QUIT: 
            done = True


# Yoloswag
pygame.quit()
